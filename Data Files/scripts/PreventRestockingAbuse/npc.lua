local self = require('openmw.self')
local core = require('openmw.core')
local types = require('openmw.types')
local nearby = require('openmw.nearby')

local common = require('scripts/PreventRestockingAbuse.common')

if not common.isMerchant(self.object) then return {} end

local function iterateStock(actor, callback)
   local actorInventory = types.Actor.inventory(actor)
   for _, item in ipairs(actorInventory:getAll()) do
      callback(item, actor)
   end
   for _, container in ipairs(nearby.containers) do
      if container.ownerRecordId == self.object.recordId then
         local containerInventory = types.Container.inventory(container)
         for _, item in ipairs(containerInventory:getAll()) do
            callback(item, container)
         end
      end
   end
end



local function copyStock(actor)
   local stock = {}
   iterateStock(actor, function(item, source)
      local itemStock = stock[item.recordId] or {}
      itemStock[source.id] = item.count
      stock[item.recordId] = itemStock
   end)
   return stock
end

local function inverseStockChanges(actor, stock)
   local changes = {}
   iterateStock(actor, function(item, source)
      local itemStock = stock[item.recordId]
      if not itemStock or not types.Item.isRestocking(item) then return end
      local sourceCount = itemStock[source.id]
      if sourceCount and item.count > sourceCount then
         table.insert(changes, {
            item = item,
            count = item.count - sourceCount,
         })
      end
   end)
   return changes
end

local savedStock = {}

return {
   engineHandlers = {
      onActive = function()
         savedStock = copyStock(self.object)
      end,
   },
   eventHandlers = {
      [common.events.Frame] = function(player)
         local changes = inverseStockChanges(self.object, savedStock)
         if #changes > 0 then
            core.sendGlobalEvent(common.events.Reset, changes)
            player:sendEvent(common.events.Update, self.object)
         end
      end,
   },
}
