
local types = require('openmw.types')






local Changes = {}

return {
   isMerchant = function(npc)
      local npcRecord = types.NPC.record(npc)
      return npcRecord.servicesOffered['Barter'] or false
   end,
   Changes = Changes,
   events = {
      Reset = 'urm_PreventRestockingAbuse_reduce',
      Frame = 'urm_PreventRestockingAbuse_frame',
      Update = 'urm_PreventRestockingAbuse_update',
   },
}
