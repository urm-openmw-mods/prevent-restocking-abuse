local common = require('scripts/PreventRestockingAbuse.common')

return {
   eventHandlers = {
      [common.events.Reset] = function(changes)
         for _, change in ipairs(changes) do
            change.item:remove(change.count)
         end
      end,
   },
}
