
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local I = require('openmw.interfaces')

local common = require('scripts/PreventRestockingAbuse.common')

return {
   engineHandlers = {
      onFrame = function(_)
         local isBartering = I.UI.getMode() == 'Barter'
         if isBartering then
            for _, actor in ipairs(nearby.actors) do
               if common.isMerchant(actor) then
                  actor:sendEvent(common.events.Frame, self.object)
               end
            end
         end
      end,
   },
   eventHandlers = {
      [common.events.Update] = function(actor)

         I.UI.removeMode('Barter')
         I.UI.addMode('Barter', { target = actor })
      end,
   },
}
