# Prevent Restocking Abuse

Prevents increasing restocking item counts. Currently has strange interactions with merchant containers.

Requires a recent [development build of OpenMW](https://openmw.org/downloads/).

To install, read the documentation on
[how to install OpenMW mods](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html?highlight=installing%20mods)
and [how to enable OpenMW scripts](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html#how-to-run-a-script)
